import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { AutorizacionService } from "./autorizacion.service";

@Injectable({
  providedIn:'root'
})
export class AutorizacionGuard implements CanActivate {

  constructor(
    private router:Router,
    private autService: AutorizacionService
    ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      console.log('GUARD',route)
    if (this.autService.isLoggedIn() || route.url.toString() === '/login') {
      return true
    } else {
      return this.router.navigateByUrl('/login')
    }
  }

}
