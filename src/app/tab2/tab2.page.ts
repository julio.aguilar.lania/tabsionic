import { Component, OnDestroy, OnInit } from '@angular/core';
import { Alumno } from '../alumnos/alumno.model';
import { AlumnosService } from '../alumnos/alumnos.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, OnDestroy {
  alumnos:Alumno[] =[]

  constructor(private alService:AlumnosService) {}

  ngOnInit(): void {
    console.log('TABDOS onInit')
      this.alService.getAlumnos()
        .subscribe(lista => {this.alumnos = lista})
  }

  ngOnDestroy(): void {
    console.log('TAB2 onDestroy')
  }

  ionViewWillEnter() {
    console.log('TABDOS will enter')
  }

  ionViewDidEnter() {
    console.log('TABDOS did enter');
  }

  ionViewWillLeave() {
    console.log('TABDOS will leave')
  }

  ionViewDidLeave() {
    console.log('TABDOS did leave')
  }

}
