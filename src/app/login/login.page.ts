import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutorizacionService } from '../autorizacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private autService: AutorizacionService,
    private router:Router) { }

  ngOnInit() {
  }

  login() {
    console.log('login en page');
    this.autService.login();
    this.router.navigateByUrl('/tabs/tab1')
  }

}
