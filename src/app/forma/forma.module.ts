import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormaPageRoutingModule } from './forma-routing.module';

import { FormaPage } from './forma.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormaPageRoutingModule
  ],
  declarations: [FormaPage]
})
export class FormaPageModule {}
