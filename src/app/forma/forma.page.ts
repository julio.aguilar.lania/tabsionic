import { Component, OnInit } from '@angular/core';
import { PickerController } from '@ionic/angular';
import { Alumno } from '../alumnos/alumno.model';

@Component({
  selector: 'app-forma',
  templateUrl: './forma.page.html',
  styleUrls: ['./forma.page.scss'],
})
export class FormaPage implements OnInit {

  alumno: Alumno | any = {}
  fechaLimite: string = ''
  constructor(private pickerCtrl: PickerController) { }
  estadoSeleccionado:number = -1

  ngOnInit() {
  }

  mostrarEstados() {
    this.pickerCtrl.create({
      columns: [
        {
        name:'estados',
        options:[
          { text:'Aguascalientes', value:1},
          { text:'Baja California', value:2},
          { text:'Baja California Sur', value:3}
        ]
        }
      ],
      buttons:[
        {
          text:'Seleccionar',
          handler:valor => { this.estadoSeleccionado = valor}
        }
      ]
    })
      .then(p => {
        p.present().then()
      })
  }

  guardar() {
    console.log('ALUMNO', this.alumno)
    console.log('ESTADO', this.estadoSeleccionado)
  }

}
