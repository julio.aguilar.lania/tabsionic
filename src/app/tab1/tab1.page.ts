import { Component, OnDestroy, OnInit } from '@angular/core';
import { Alumno } from '../alumnos/alumno.model';
import { AlumnosService } from '../alumnos/alumnos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy {
  alumnos:Alumno[] =[]

  constructor(private alService:AlumnosService) {}

  ngOnInit(): void {
    console.log('TAB1 onInit')
      this.alService.getAlumnos()
        .subscribe(lista => {this.alumnos = lista})
  }

  ngOnDestroy(): void {
      console.log('TAB1 onDestroy')
  }

  ionViewWillEnter() {
    console.log('TAB1 will enter')
  }

  ionViewDidEnter() {
    console.log('TAB1 did enter');
  }

  ionViewWillLeave() {
    console.log('TAB1 will leave')
  }

  ionViewDidLeave() {
    console.log('TAB1 did leave')
  }

}
