export interface Alumno {
  nombre: string,
  matricula: string,
  fechaNacimiento: Date,
  promedio: number,
  activo: boolean
}
