import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Alumno } from "./alumno.model";

@Injectable({
  providedIn:'root'
})
export class AlumnosService {
  listaAlumnos: Alumno[] = [{
    nombre:'Fulano Fernández',
    matricula:'202201928A',
    fechaNacimiento:new Date('2000-01-01'),
    promedio:8.2,
    activo:true
  },
  {
    nombre:'Mengano Méndez',
    matricula:'202201929A',
    fechaNacimiento:new Date('2000-02-02'),
    promedio:7.8,
    activo:true
  },
  {
    nombre:'Perengano Pérez',
    matricula:'202100872B',
    fechaNacimiento:new Date('1999-03-03'),
    promedio:9.1,
    activo:true
  },
]

  getAlumnos() : Observable<Alumno[]> {
    return of(this.listaAlumnos)
  }

  getAlumnoPorMatricula(mat: string) : Observable<Alumno> {
    return of(this.listaAlumnos.filter(a => {a.matricula == mat})[0])
  }

  guardarAlumno(al : Alumno) : Observable<Alumno> {
    this.listaAlumnos.push(al);
    return of(al)
  }
}
