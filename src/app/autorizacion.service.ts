import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {

  private estatusLoggedIn = false
  constructor() { }

  isLoggedIn() : boolean {
    return this.estatusLoggedIn
  }

  login() : void {
    this.estatusLoggedIn = true
  }

  logout() : void {
    this.estatusLoggedIn = false
  }
}
