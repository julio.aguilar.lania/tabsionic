import { Component, OnInit } from '@angular/core';
import { Device } from '@capacitor/device';
import { DeviceInfo } from '@capacitor/device/dist/esm/definitions';

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
})
export class DevicePage implements OnInit {

  infoDispositivo: DeviceInfo | any
  constructor() { }

  ngOnInit() {
    Device.getInfo()
      .then(info => {
        console.log(info)
        this.infoDispositivo = info
      })
  }

}
