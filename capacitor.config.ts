import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'mx.lania.ejemplotabstsadm2023',
  appName: 'Tabs',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
